//[SECTION] While Loop
//If the condition evaluates to true, the statements inside the code block will executed
/*
	Syntax: 
	while(expression /condition){
		statements
	}
*/


//while the value of count is not equal to 0
let count = 0;

while(count !==5){
	console.log("While:" + count);

	//Iteration - It increase the values of count after every iteration to stop the loop when the it reaches 5

	// ++ increment, -- decrement
	count++;
}

//[SECTION] Do While Loop

//A do-while loop works a lot like the while loop. But unlike while loops, Do-while loops guarantee that the code will be executed atleast once.

/*
	Syntax:
	do {
		statement 
	} while(expression/condition)
*/

let number = Number(prompt("Give me a number"));

do {
	console.log("do  while: " + number);
	number += 1;
}while (number < 10);


//[SECTION ] for Loops
/*
	Syntax:
	for(initialization; expression/condition; finalExpression){
		statement
	}
*/

for (let count = 0; count <= 20; count ++) {
	console.log(count);
}


let myString = "Alex";

//Characyer in strings may be counted using the .length property. 

console.log(myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
//Will create a lopop that will print out the individual letters  of a variable

for (let x = 0; < myString.length; x++){

	//The current value of my string is printed out using it's index value
	console.log(myString[x]);
}

// Changing of vowels using loops

let myName = "DelIZo";

for (let i = 0; i < myName.length; i++){
    if (
        myName[i].toLowerCase() == "a" ||
        myName[i].toLowerCase() == "i" ||
        myName[i].toLowerCase() == "u" ||
        myName[i].toLowerCase() == "e" ||
        myName[i].toLowerCase() == "o" 
    ){
        console.log(3);
    } else {
        console.log(myName[i]);
    }
}

Kimberly Hernandez, Now
// Changing of vowels using loops

//of the character of yourname is a vowel letter, instead of displayeding the character, displayed number 3
let myName = "DelIZo";

for (let i = 0; i < myName.length; i++){
 if (
 myName[i].toLowerCase() == "a" ||
 myName[i].toLowerCase() == "i" ||
 myName[i].toLowerCase() == "u" ||
 myName[i].toLowerCase() == "e" ||
 myName[i].toLowerCase() == "o" 
 ){
 console.log(3);
} else {
 console.log(myName[i]);
 }
}

